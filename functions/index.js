'use strict';
const functions = require('firebase-functions');
const BootBot = require('bootbot');
const express = require('express');

const bot = new BootBot({
  accessToken: 'EAAEFhSDDSCkBAHMMK517kEDhN3UhZA212zWnfXLUAoLAr4F8bN47Vdkjp5sDcjabJvwJRbkqiiNG0XCeixctB7SiyB4ipmttp15HJ77rkpbOTf3b8Et9yEJDZCDK5CRZBteZBZAS1kMJ4o7nOdwMM4z8iYpJBAdAcGKLZA67CAfnZCaKjql2JA1',
  verifyToken: 'nugUalA0WS', // gerado por random.org
  appSecret: 'f498d4ad70fcf711581c6e2b34d4e84f'
});

// bot.on('message', (payload, chat) => {
//   console.log(JSON.stringify(payload))
//   const text = payload.message.text;
//   chat.say(`Echo: ${text}`);
// });

bot.hear(['hello', 'hi', /hey( there)?/i], (payload, chat) => {
  console.log('The user said "hello", "hi", "hey", or "hey there"');
});

bot.hear(['peso', 'to gordo?', /hey( there)?/i], (payload, chat) => {
  console.log('The user said "hello", "hi", "hey", or "hey there"');
  chat.say(`Sua última pesagem foi feita no dia 02/06/2018 com o peso 68.3kg`);
});

bot.hear('image', (payload, chat) => {
	// Send an attachment
	chat.say({
		attachment: 'image',
    url: `https://69c753ba.ngrok.io/scale.jpeg`
	});
});

bot.app.use(express.static('public'))
bot.start();

const api = functions.https.onRequest((request, response) => {
  if (!request.path) {
    request.url = `/${request.url}` // prepend '/' to keep query params if any
  }
  return bot.app(request, response)
})

module.exports = {
  api
}